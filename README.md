# Previsão do Tempo

Video Apresentação do Teste
[![Teste Python/React]](https://youtu.be/8rwwd8zHF4k)

Artefato Entrega
https://www.youtube.com/watch?v=8rwwd8zHF4k&t=9s

A React and Django web-app that display weather based on the city entered by the user.

Inform the city
![picture](readme-images/img4.png)

After informing the city - Submit
![picture](readme-images/img5.png)

Result
![picture](readme-images/img6.png)

Postman
![picture](readme-images/img1.png)

API Python(Django RestFull)
![picture](readme-images/img2.png)

API Python(Django RestFull) Historico Gravado
![picture](readme-images/img3.png)

## Installation Instructions

- After cloning the repository, `cd` into it and run following commands:

```
# Create a virtual environment for the backend
virtualenv env

# Activate the virtual environment
source env/bin/activate

# Install the packages
pip install -r requirements.txt


# cd into the frontend directory
cd frontend

# Install packages for the React app
yarn install

source env/bin/activate
cd backend

# To run Django dev server and watch file changes in the app
./manage.py runserver
```


## Technical Details

### Code Architecture

#### Overview

- `frontend` directory:
    - React app, which can be run separately on its own by running `npm start` in the `frontend` directory, after installing the node_modules.
- `backend` directory:
    - Main Django app, on which the whole application is running.
- `env`: 
    - Virtual environment for the Django project
- `requirements.txt`:
    - Dependency list of the Django app

#### Workflow

- Inside `frontend/package.json`, npm-watch module is included as a dev dependency because it will detect changes in the React app (`frontend/src` and `frontend/public`) and `buildapp` will run `build.sh` script.
- Every time `build.sh` script runs, it will generate static bundle files from the React app and `./manage.py collectstatic` will collect those static files and store them inside `backend/static`
- Whitenoise:
    - This package is used to  serve and cache static files from `frontend/build/root/` whenever needed.
- Files stored at `frontend/build/static` will be served at `/static/` URL, settings for the same are specified in `backend/weatherfinder/settings.py`.
- Django's `urls.py` will go to `views.py`, which will render `index.html` file of React app.
- This `index.html` when rendered, will have links to static JS bundle files and they'll be considered as Django templates and served from there.
- Nodemon:
    - Keeps track of changes being made in the file and reruns the file or server based on changes in the source code.


### APIs Used:

#### [OpenWeatherMap API](https://community-open-weather-map.p.rapidapi.com/forecast)

- To display weather based on city.

