from django.urls import path, include
from rest_framework import routers, serializers, viewsets
from api.models import *
from api.views import *

class HistoricoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Historico
        fields = "__all__"

class HistoricoViewSet(viewsets.ModelViewSet):
    queryset = Historico.objects.all()
    serializer_class = HistoricoSerializer

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'historicos', HistoricoViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('lerdados/',lerDados,name='lerdados'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]