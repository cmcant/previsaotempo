from django.db import models

class Historico(models.Model):
    log_text = models.CharField(max_length=4000)    

    def __str__(self):
        return "%s" % (self.log_text)
