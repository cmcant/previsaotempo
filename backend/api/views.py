from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
import urllib3
import certifi
import json
from api.models import *

@csrf_exempt
def lerDados(request):
    a = []
    if (request.method == "POST"):
        body_unicode = eval(request.body.decode('utf-8'))
        ccyte = body_unicode['cyte']

        http = urllib3.PoolManager(
        cert_reqs='CERT_REQUIRED',
        ca_certs=certifi.where()
        )

        url = "https://community-open-weather-map.p.rapidapi.com/forecast?q=%s" % (ccyte)
        r = http.request(
            'GET',
            url,
            headers={
                'x-rapidapi-key': '7c61934bbemshb158232b4267a9ap13b0edjsn2e46161646d8',
                'x-rapidapi-host':'community-open-weather-map.p.rapidapi.com'
            }
        )
        r1 = eval(r.data)
        a.append(r1)
        print(r1['cod'])

        Historico.objects.create(log_text=r1)
        
    return HttpResponse(json.dumps(a), content_type="application/json", status=r1['cod'])



