import './App.css';
import Buscar from './components/buscarform';
function App() {
  
  return (
    <div className="App">
      <header className="App-header">
        <h1>PREVISÃO DO TEMPO</h1>

        <Buscar/>

      </header>
    </div>
  );
}

export default App;
