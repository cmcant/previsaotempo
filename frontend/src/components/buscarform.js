import React from 'react';
import axios from 'axios';

class Buscar extends React.Component {

  constructor() {
    super();
    this.state = {
      city: "",
      country: "",
      data:[]
    };
  }

  myChangeHandler = (event) => {
    this.setState({data: []});
    this.setState({city: event.target.value});
    this.setState({country: ""});
  }
  myFocusHandler = (event) => {
    this.setState({city: ""});    
    this.setState({country: ""});
  }

  mySubmitHandler = (event) => {
    event.preventDefault();
    axios.post('http://localhost:8000/lerdados/',{"cyte":this.state.city})
    .then(res => {
                
      this.setState({city: res.data[0]["city"]["name"]});
      this.setState({country: res.data[0]["city"]["country"]});
      this.setState({data: res.data[0]["list"]});
           
      }).catch(error => {
        alert("Cidade " + this.state.city + " não localizada! " )
      })
  }
    

  render() {
    
    const items = this.state.data.map((item) =>
     <div>

        <div key={item.dt} class="c-box">
        <p class="c-box__content">Data: {item.dt_txt}</p>
        <p>Temperatura: {item.main.temp}</p>
        <p>Temperatura Maxíma: {item.main.temp_max}</p>
        <p>Temperatura Mínima: {item.main.temp_min}</p>
        </div>
       
      </div>
    );
    return <div>

    <form onSubmit={this.mySubmitHandler}>
    <p>Digite a Cidade:</p>
    <input
      type='text'
      onChange={this.myChangeHandler}
      onFocus={this.myChangeHandler}
    />

    <input type='submit'/>
    
    </form>
    
    <div>
      <h1>{this.state.city}/{this.state.country}</h1>
    </div>

    <div>{items}</div>
    </div>
  }
}
export default Buscar